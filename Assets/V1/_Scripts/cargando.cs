﻿using UnityEngine;

public class cargando : MonoBehaviour
{
    private bool mChangeLevel = true;

    [SerializeField] private float timeToFade = 300;
    [SerializeField] private string scene;
    [SerializeField] private Color loadToColor = Color.white;

    private float timer = 1f;

    private void FixedUpdate()
    {
        timer++;
    }

    void Update()
    {
        if (mChangeLevel && timer > timeToFade)
        {
            LoadNextSceneAsync();
            mChangeLevel = false;
        }
    }

    private void LoadNextSceneAsync()
    {
        Application.backgroundLoadingPriority = ThreadPriority.High;
        Initiate.Fade(scene, loadToColor, 1);
    }
}