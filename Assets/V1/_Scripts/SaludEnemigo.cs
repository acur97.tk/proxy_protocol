﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SaludEnemigo : MonoBehaviour
{
    public Slider SliderVidaMalo;
    public float Vida = 100f;
    public Animator AnimatorEnemy;
    public GameObject explocion;

    private bool asesinado = false;

    private readonly string _Sword = "Sword";
    private readonly string _Muerto = "Muerto";

    void starFalso()
    {
        AnimatorEnemy = GetComponent<Animator>();
        asesinado = false;
    }

    void Start()
    {
        SliderVidaMalo.value = Vida;
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == _Sword)
        {
            SliderVidaMalo.value -= 50f;
        }
    }

    void FixedUpdate()
    {
        if (SliderVidaMalo.value == 0)
        {
            asesinado = true;
            explocion.SetActive(true);
        }
        if (asesinado == true)
        {
            AnimatorEnemy.SetTrigger(_Muerto);
            StartCoroutine(nameof(Matado));
        }
    }

    IEnumerator Matado()
    {
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }
}