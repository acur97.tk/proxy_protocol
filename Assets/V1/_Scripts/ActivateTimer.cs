﻿using UnityEngine;
using UnityEngine.UI;

public class ActivateTimer : MonoBehaviour
{
    private float timer = 30f;
    public Text timerSeconds;
    private bool fadeando = false;

    [Space]
    [SerializeField] private string scene;
    [SerializeField] private Color loadToColor = Color.white;

    void Start()
    {
        timerSeconds.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        string minutes = ((int)timer / 60).ToString();
        string seconds = (timer % 60).ToString("f0");
        timerSeconds.text = minutes + " : " + seconds;

        if (timer <= 0 && !fadeando)
        {
            fadeando = true;
            Application.backgroundLoadingPriority = ThreadPriority.Low;
            Initiate.Fade(scene, loadToColor, 1);
        }
    }
}