﻿using UnityEngine;
using UnityEngine.Video;

public class Skip1 : MonoBehaviour
{
    public GameObject texto;
    private float clics;
    public VideoPlayer video;

    private void Start()
    {
        clics = 0;
        texto.SetActive(false);
    }

    private void LateUpdate()
    {
        if (video.isPlaying == false)
        {
            LoadNextSceneAsync();
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            clics++;
            texto.SetActive(true);
        }

        if (clics == 2)
        {
            LoadNextSceneAsync();
        }
    }

    private void LoadNextSceneAsync()
    {
        Application.backgroundLoadingPriority = ThreadPriority.Low;
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(0);
        clics = 0;
    }
}