﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Salud : MonoBehaviour
{
    public Slider SliderVida;
    public Slider SliderStamina;
    public float VidaKyra = 100f;
    public float PorcentajeEstamina = 1000f;
    public GameObject UI;
    private float timer = 1f;
    public float TiempoEsperaMuerto = 20f;
    public GameObject VideoMuerte;
    public Animator AnimatorKyla;

    [Range(0, 1000)]
    private float timer2 = 1f;

    private readonly string _Muerta = "Muerta";

    void startFalso()
    {
        AnimatorKyla = GetComponent<Animator>();
    }

    void Start()
    {
        SliderVida.value = VidaKyra;
        SliderStamina.value = PorcentajeEstamina;
    }

    void FixedUpdate()
    {
        if (SliderVida.value == 0)
        {
            Time.timeScale = 0.05f;
            AnimatorKyla.SetTrigger(_Muerta);
            VideoMuerte.SetActive(true);
            UI.SetActive(false);

            timer++;
            if (timer == TiempoEsperaMuerto)
            {
                timer = 1f;
                Scene scene = SceneManager.GetActiveScene();
                SceneManager.LoadScene(scene.name);
                Time.timeScale = 1f;
            }
        }
        else
        {
            UI.SetActive(true);
            VideoMuerte.SetActive(false);
        }

        if (SliderStamina.value < 1000)
        {
            timer2++;
            SliderStamina.value++;
        }
    }
}