﻿using UnityEngine;
using UnityEngine.AI;

public class DronDetector : MonoBehaviour
{
    Transform pillada;
    public string TagKyla = "Kyla";
    public NavMeshAgent NavDelDron;
    bool Detectada = false;
    public Light LuzDelDron;
    public Animator AnimDron;

    public bool estaDisparando = false;
    public Bala bala;
    public float velocidadBala;
    public float delayDeDisparos;
    float contadorBalas;
    public Transform PosicionBala;

    public Transform GuardiaDron;

    public float VistaDron = 15f;
    public float VistaPuntoGuardia = 2f;

    private Ray RayoAmenazador;
    private RaycastHit hit;
    private Bala newbala;
    private Color _col = new Color32(203, 158, 116, 255);

    void Start()
    {
        pillada = GameObject.FindGameObjectWithTag(TagKyla).transform;
        NavDelDron = GetComponent<NavMeshAgent>();
        LuzDelDron = GetComponent<Light>();
        AnimDron = GetComponent<Animator>();
        AnimDron.enabled = true;
    }

    void FixedUpdate()
    {
        estaDisparando = false;

        RayoAmenazador = new(transform.position + Vector3.down, transform.forward); //Enlazar el rayo con el objeto
        Debug.DrawRay(RayoAmenazador.origin, RayoAmenazador.direction, Color.red);

        if (Physics.Raycast(RayoAmenazador, out hit, VistaDron))
        {
            if (hit.collider.CompareTag(TagKyla))
            {
                estaDisparando = true;

                if (estaDisparando)
                {
                    contadorBalas -= Time.deltaTime;
                    if (contadorBalas <= 0)
                    {
                        contadorBalas = delayDeDisparos;
                        newbala = Instantiate(bala, PosicionBala.position, PosicionBala.rotation);
                        newbala.velocidad = velocidadBala;
                    }
                    else
                    {
                        contadorBalas = 0;
                    }
                }
            }
        }

        if (Physics.Raycast(RayoAmenazador, out hit, VistaPuntoGuardia))
        {
            if (hit.collider.name == GuardiaDron.name)
            {
                AnimDron.enabled = true;
            }
        }

        if (Detectada == true)
        {
            AnimDron.enabled = false;
            transform.LookAt(pillada);
            LuzDelDron.color = Color.red;
            NavDelDron.SetDestination(pillada.position);
        }

        if (Detectada == false)
        {
            LuzDelDron.color = _col;
            NavDelDron.SetDestination(GuardiaDron.position);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(TagKyla))
        {
            Detectada = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag(TagKyla))
        {
            Detectada = false;
        }
    }
}