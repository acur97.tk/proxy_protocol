﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Bala : MonoBehaviour
{
    private GameObject _SliderVida;
    private Slider SliderVida;
    public float Dolor = 10;
    public string tagDeKyla = "Kyla";
    public float velocidad;

    private readonly string _SVida = "SVida";
    private readonly string _Piso = "Piso";
    private readonly WaitForSeconds wait = new(0.5f);

    // Use this for initialization
    void Start()
    {
        _SliderVida = GameObject.Find(_SVida);
        if (_SliderVida != null)
        {
            SliderVida = _SliderVida.GetComponent<Slider>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Time.deltaTime * velocidad * Vector3.forward);
        StartCoroutine(AutoDestruccionBala());
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(tagDeKyla))
        {
            if (_SliderVida != null)
            {
                SliderVida.value -= (Dolor / 10);
            }
            Destroy(gameObject);
        }
        if (other.gameObject.CompareTag(_Piso))
        {
            Destroy(gameObject);
        }
    }

    IEnumerator AutoDestruccionBala()
    {
        yield return wait;
        Destroy(gameObject);
    }
}